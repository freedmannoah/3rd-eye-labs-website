$(document).ready(function () {
    var leftFlask = $('.panel-1-flask');
    var panel1content = $('#panel-1-content');

    //click listeners for nav
    $('.nav-button').click(function(e) {
        var id = e.currentTarget.id;
        switch (id) {
            case 'ourwork':
                animateTransition();
                break;
        }
    });
    $('.panel-button').click(function(e) {
       var panel = $(e.currentTarget).data('panel');
       $('.panel-button').removeClass('active');
       $(e.currentTarget).addClass('active');
       $('.panel-content').removeClass('active');
       $('#' + panel).addClass('active');
    });


    function animateIn() {
        leftFlask.transition({
            y: '564px'
        }, 2100, function() {
            leftFlask.hide();
        });
    }
    function animateTransition() {
        leftFlask.show();
        leftFlask.transition({
            y: '0px',
            easing: 'in'
        }, 500, function() {
            panel1content.html('<h1>Our work!</h1>');
            animateIn();
        });
    }

    function sizeVideo() {
        var width = $(document).width();
        if (width < 1115) {
            $('.background-vid').width('auto');
            $('.background-vid').height(501);
        } else {
            $('.background-vid').width('100%');
            $('.background-vid').height('auto');
        }
    }

    $(window).load(function() {
        //setTimeout(animateIn, 600);
        //set video size
        //sizeVideo();
        //$(window).on('resize', sizeVideo);
    });
});
