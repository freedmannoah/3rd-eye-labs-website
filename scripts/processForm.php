<?php

if(!$_POST) {
    header("Location: /contact.html");
    exit;
} else {
    $name = (isset($_POST['name'])) ? $_POST['name'] : NULL;
    $email = (isset($_POST['email'])) ? $_POST['email'] : NULL;
    $phone = (isset($_POST['phone'])) ? $_POST['phone'] : NULL;
    $inquiry = (isset($_POST['inquiry'])) ? $_POST['inquiry'] : NULL;
}

$email = preg_replace("([\r\n])", "", $email);

$find = "/(content-type|bcc:|cc:)/i";
if (preg_match($find, $name) || preg_match($find, $email) || preg_match($find, $phone) || preg_match($find, $inquiry)) {
    echo "<h1>Error</h1>\n
      <p>No meta/header injections, please.</p>";
    exit;
}

// multiple recipients (note the commas)
$to = "noah@3rdeyelabs.com, ";
$to .= "freedmannoah@gmail.com";

// subject
$subject = "New Client Request - www.3rdeyelabs.com";


// compose message
$message = "
<html>
  <head>
    <title>New Client Request</title>
  </head>
  <body>
    <h1>New client for 3rd eye labs</h1>
    <p>Name: {$name}</p>
    <p>Email: {$email}</p>
    <p>Phone: {$phone}</p>
    <p>Inquiry: {$inquiry}</p>
  </body>
</html>
";


// To send HTML mail, the Content-type header must be set
$headers = 'From: info@3rdeyelabs.com\r\n';
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

// send email
mail($to, $subject, $message, $headers);
?>